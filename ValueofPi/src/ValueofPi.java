
public class ValueofPi
{

	public static void main(String[] args)
	{
		double value = 0;
		int valueAdded = 1;
		
		//System.out.printf("%.2f",(Math.floor(5.0505*100)/100));
						
		System.out.print("Iteration -- Value of Pi\n");
		
		for(int i=1;i<2000;i++)
		{  
			
			if(i%2==1)
			{
				value = value + (4.00000) /valueAdded;
			}
			
			if(i%2==0)
			{
				value = value - (4.00000) /valueAdded;
			}
			
			System.out.printf("%5d %15.8f\n", i, value);
			valueAdded+=2;
			
		}
		

	}

}
